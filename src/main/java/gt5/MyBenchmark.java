package gt5;

import org.openjdk.jmh.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Fork(value = 1, jvmArgs = {"-Xms20G", "-Xmx20G"})
@Warmup(iterations = 5)
@Measurement(iterations = 10)
//@BenchmarkMode({Mode.Throughput}) @OutputTimeUnit(TimeUnit.SECONDS)
@BenchmarkMode({Mode.SampleTime}) @OutputTimeUnit(TimeUnit.NANOSECONDS)
public class MyBenchmark {

    @State(Scope.Benchmark)
    public static class MyState {

        @Param({"10", "100", "1000", "100000", "1000000", "10000000", "100000000"})
        private int size;

        private Integer result;

        Integer[] array;
        Stream<Integer> stream1;
        Stream<Integer> stream2;
        List<Integer> list;

        @Setup(Level.Invocation)
        public void setup() {
            System.gc();
            result = 0;
            array = new Integer[size];
            for (int i = 0; i < array.length; i++) {
                array[i] = i;
            }
            stream1 = IntStream.range(0, size).boxed();
            stream2 = IntStream.range(0, size).boxed();
            list = IntStream.range(0, size).boxed().collect(toList());
        }
    }

    @Benchmark
    public int forLoop(MyState state) {
        for (int i = 0; i < state.array.length; i++) {
            state.result = i;
        }
        return state.result;
    }

    @Benchmark
    public int forIter(MyState state) {
        for (Integer i : state.list) {
            state.result = i;
        }
        return state.result;
    }

    @Benchmark
    public int listForEach(MyState state) {
        state.list.forEach(i -> state.result++);
        return state.result;
    }

    @Benchmark
    public int listStreamForEach(MyState state) {
        state.list.stream().forEach(i -> state.result = i);
        return state.result;
    }

    @Benchmark
    public int streamForEach(MyState state) {
        state.stream1.forEach(i -> state.result = i);
        return state.result;
    }

    @Benchmark
    public int streamParallelForEach(MyState state) {
        state.stream2.parallel().forEach(i -> state.result = i);
        return state.result;
    }

}
